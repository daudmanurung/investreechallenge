package IslandArea;

import java.util.Scanner;

public class Posisi {

    public final char[] mapY = new char[] {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'};
    public final int[] mapX = new int[] {1,2,3,4,5,6,7,8,9,10};

    int posisiX, posisiY;

    public Posisi(int posisiX, int posisiY){
        this.posisiX = posisiX;
        this.posisiY = posisiY;
    }


    public int getPosisiX() {
        return posisiX;
    }

    public void setPosisiX(int posisiX) {
        this.posisiX = posisiX;
    }

    public int getPosisiY() {
        return posisiY;
    }

    public void setPosisiY(int posisiY) {
        this.posisiY = posisiY;
    }

    public void PosisiSekarang(){
        System.out.println("Sekarang kamu berapa pada titik: " + mapX[posisiX]+mapY[posisiY]);
    }

    public void Move(int pindah){
        Scanner scanner = new Scanner(System.in);
        int pilih, jarak;
        System.out.println("Mau bergerak ke arah mana? ");
        System.out.println("1. North\n2. West\n3. East\n4. South");

        System.out.println("Pilihan Kamu: ");
        pilih = scanner.nextInt();
        switch (pilih){

            case 1:
                if(posisiY == 0){
                    System.out.println("Kamu sudah di titik itu");
                }else {
                    posisiY -= pindah;
                    setPosisiY(posisiY);
                }
                break;
            case 2:
                if(posisiX == 0 ){
                    System.out.println("Here");
                }else {
                    posisiX -= pindah;
                    setPosisiX(posisiX);
                }
                break;
            case 3:
                if(posisiX == 0){
                    System.out.println("Here");
                }else {
                    posisiX += pindah;
                    setPosisiX(posisiX);

                }
                break;
            case 4:
                if (posisiY == 0){
                    System.out.println("Here");
                }else {
                    posisiY += pindah;
                    setPosisiY(posisiY);
                }
                break;
        }


    }

    public void pullOver(){
        Scanner scanner = new Scanner(System.in);
        int pilih;
        System.out.println("Mau bergerak ke arah mana? ");
        System.out.println("1. North\n2. West\n3. East\n4. South");

        System.out.println("Pilihan Kamu: ");
        pilih = scanner.nextInt();
        switch (pilih){
            case 1:
                posisiY = 0;
                break;
            case 2:
                posisiX = 0;

                break;
            case 3:
               posisiX = 9;
                break;
            case 4:
                posisiY = 9;
                break;
        }
    }

    public void reset(){
        posisiX = 4;
        posisiY = 5;

    }
}