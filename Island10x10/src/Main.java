import IslandArea.Posisi;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Posisi p = new Posisi(4,5);
        p.PosisiSekarang();

        int pilihan, jarak;
        Scanner scanner = new Scanner(System.in);

        System.out.println("1. Move\n2. Pull Over\n3. Restart");
        System.out.println("Pilih Aksi: ");
        pilihan = scanner.nextInt();

        while (pilihan != 0){

            switch (pilihan){
                case 1:
                    System.out.println("Jarak: ");
                    jarak = scanner.nextInt();

                    p.Move(jarak);
                    p.PosisiSekarang();
                    break;
                case 2:
                    p.pullOver();
                    p.PosisiSekarang();
                    break;

                case 3:
                    p.reset();
                    break;
            }
        }



    }
}
