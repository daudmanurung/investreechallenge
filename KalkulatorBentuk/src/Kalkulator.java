import BangunDatar.Segitiga;

import java.util.Scanner;

public class Kalkulator {
    public static void main(String[] args) {
        Integer myOption=null;
        double alas, tinggi, luas;

        Segitiga segitiga1;
        Scanner scanner = new Scanner(System.in);

        System.out.println("1. Hitung Luas Segitiga");
        System.out.println("2. Hitung Volume Balok");
        System.out.println("3. Hitung Volume Kubus");

        System.out.print("Saya memilih: ");
        myOption = scanner.nextInt();

        switch (myOption){
            case 1:
                System.out.println("Kita hitung Luas segitiga:\n");
                System.out.println("Masukkan alas: ");
                alas = scanner.nextDouble();
                System.out.println("Masukkan Tinggi: ");
                tinggi = scanner.nextDouble();

                segitiga1 = new Segitiga(alas, tinggi);

                luas = segitiga1.HitungLuasSegitiga();

                System.out.println("Luas Segitiga kamu adalah: " + luas);
                break;

            case 2:
                System.out.println("Kita hitung Volume Balok ");
        }

    }
}
