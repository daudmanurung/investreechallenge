package com.siswa.siswa.repository

import com.siswa.siswa.entity.Siswa
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface SiswaRepository: JpaRepository<Siswa, Int> {
}