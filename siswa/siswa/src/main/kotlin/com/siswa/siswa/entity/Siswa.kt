package com.siswa.siswa.entity

import javax.persistence.*

import javax.persistence.*

@Entity
@Table(name = "siswa")
data class Siswa(
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Int? = null,

    @Column(name = "nama")
    var nama: String? = null,

    @Column(name = "alamat")
    var alamat: String? = null,

    @Column(name = "jeniskelamin")
    var jeniskelamin: String? = null,

    @Column(name = "umur")
    var umur: Int? = null


)