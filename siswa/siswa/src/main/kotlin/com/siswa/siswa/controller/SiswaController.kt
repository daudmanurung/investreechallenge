package com.siswa.siswa.controller

import com.siswa.siswa.entity.Siswa
import com.siswa.siswa.service.SiswaService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping

@Controller
@RequestMapping(value = ["/siswa"], produces = [MediaType.APPLICATION_JSON_VALUE])
class SiswaController @Autowired constructor(
    private val siswaService: SiswaService
) {
    @GetMapping
    fun getAllSiswa(): ResponseEntity<List<Siswa>>{
        return ResponseEntity(siswaService.getSiswa(), HttpStatus.OK)
    }

    @PostMapping
    fun addSiswa(
        @RequestBody siswa: Siswa
    ): ResponseEntity<Siswa>{
        return ResponseEntity(siswaService.addSiswa(siswa), HttpStatus.OK)
    }

    @PutMapping("{id}")
    fun updateSiswa(
        @PathVariable("id") id: Int,
        @RequestBody siswa: Siswa
    ): ResponseEntity<Siswa>{
        return ResponseEntity(siswaService.updateSiswa(id, siswa ), HttpStatus.OK)
    }

    @DeleteMapping("{id}")
    fun deleteSIswa(
        @PathVariable("id") id: Int
    ): ResponseEntity<String>{
        return ResponseEntity(siswaService.deleteSiswa(id), HttpStatus.OK)
    }
}