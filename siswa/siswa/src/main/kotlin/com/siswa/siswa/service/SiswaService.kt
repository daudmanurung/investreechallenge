package com.siswa.siswa.service

import com.siswa.siswa.entity.Siswa
import com.siswa.siswa.repository.SiswaRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class SiswaService @Autowired constructor(
    private val siswaRepository: SiswaRepository
) {
    fun getSiswa(): List<Siswa>{
        return siswaRepository.findAll()
    }

    fun addSiswa(siswa: Siswa): Siswa{
        return siswaRepository.save(siswa)
    }

    fun updateSiswa(id: Int, siswa: Siswa): Siswa{
        val updateSiswa = siswaRepository.findById(id).get()
        updateSiswa.nama = siswa.nama
        updateSiswa.alamat = siswa.alamat
        updateSiswa.jeniskelamin = siswa.jeniskelamin
        updateSiswa.umur = siswa.umur

        return siswaRepository.save(updateSiswa)
    }

    fun deleteSiswa(id: Int): String {
        val checkSiswa = siswaRepository.findById(id)
        if (checkSiswa == null){
            throw Exception()
        }else{
            siswaRepository.deleteById(id)
            return ("Data siswa dengan id $id telah di hapus")
        }

    }


}